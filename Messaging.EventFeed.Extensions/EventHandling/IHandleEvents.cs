﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messaging.EventFeed.Extensions.EventHandling
{
    public interface IHandleEvents : IDisposable
    {
        void Handle(Event evnt);

        void HandleError(SubscribeError error);

        string ForChannel();
    }
}
