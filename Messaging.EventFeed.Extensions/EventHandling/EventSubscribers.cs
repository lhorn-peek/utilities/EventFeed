﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messaging.EventFeed.Interface;

namespace Messaging.EventFeed.Extensions.EventHandling
{
    public class EventSubscribers : IEventSubscribers
    {
        #region init

        private readonly IEventHub _eventHub;
        private readonly IHandleEvents[] _handlers;

        public EventSubscribers(IEventHub eventHub, IHandleEvents[] handlers)
        {
            _eventHub = eventHub;
            _handlers = handlers;
        }

        #endregion

        public void Register()
        {
            foreach (var handler in _handlers)
            {
                _eventHub.Listen(handler.ForChannel(),
                    evnt =>
                    {
                        handler.Handle(evnt);
                    },
                    error =>
                    {
                        handler.HandleError(error);
                    });
            }
        }

    }
}
