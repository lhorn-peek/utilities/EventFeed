﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messaging.EventFeed.Interface;
using Microsoft.Extensions.DependencyInjection;

namespace Messaging.EventFeed.Extensions.EventHandling
{
    public class EventSubscribersAspCore : IEventSubscribers
    {
        #region init

        private readonly IEventHub _eventHub;
        private readonly EventHubChannels _channels;
        private readonly IServiceProvider _serviceProvider;

        public EventSubscribersAspCore(IEventHub eventHub, EventHubChannels channels, IServiceProvider serviceProvider)
        {
            _eventHub = eventHub;
            _channels = channels;
            _serviceProvider = serviceProvider;
        }

        #endregion

        public void Register()
        {
            foreach (var channel in _channels.Channels)
            {
                _eventHub.Listen(channel,
                    evnt =>
                    {
                        using (var handler = _serviceProvider.GetServices<IHandleEvents>() // improve this!!! possible leak. create aspcore DI service factory
                            .FirstOrDefault(h => h.ForChannel() == channel))
                        {
                            handler.Handle(evnt);
                            handler.Dispose();
                        }
                    },
                    error =>
                    {

                    });
            }
        }
    }
}
