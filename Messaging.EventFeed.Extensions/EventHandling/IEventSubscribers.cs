﻿namespace Messaging.EventFeed.Extensions.EventHandling
{
    public interface IEventSubscribers
    {
        void Register();
    }
}