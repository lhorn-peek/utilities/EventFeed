﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messaging.EventFeed.Extensions.EventHandling
{
    public class EventHubChannels
    {
        public string[] Channels { get; set; }
    }
}
