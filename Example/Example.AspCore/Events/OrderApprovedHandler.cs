﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Messaging.EventFeed;
using Messaging.EventFeed.Extensions.EventHandling;

namespace Example.AspCore.Events
{
    public class OrderApprovedHandler : IHandleEvents
    {
        private bool _disposed;

        public void Handle(Event evnt)
        {
            Debug.WriteLine("Recieved 'OrderApprovedEvent'");
        }

        public void HandleError(SubscribeError error)
        {
            Debug.WriteLine("Error in handling event");
        }

        public string ForChannel()
        {
            return "OrderApprovedChannel";
        }

        #region dispose

        ~OrderApprovedHandler()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // free other managed objects that implement IDisposable only, ie: '_someObjectRef.Dispose()'
            }

            //handle unmanaged resources, ie: '_someService = null;'

            _disposed = true;
        }

        #endregion
    }
}
