﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Example.AspCore.Events;
using Messaging.EventFeed;
using Messaging.EventFeed.Extensions.EventHandling;
using Messaging.EventFeed.Interface;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Example.AspCore
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddTransient<IHandleEvents, OrderApprovedHandler>();
            services.AddTransient<IHandleEvents, OrderRecievedHandler>();

            services.AddTransient(p => p.GetServices<IHandleEvents>().ToArray());

            services.AddScoped<IEventHub>(_ => new EventHub(Configuration["Data:EventHub:pubkey"], Configuration["Data:EventHub:subkey"]));

            services.ConfigurePOCO<EventHubChannels>(Configuration.GetSection("Data:EventHubChannels"));

            services.AddScoped<IEventSubscribers, EventSubscribersAspCore>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime applicationLifetime)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            //REGISTERS EVENT HANDLERS

            var eventSubscribers = app.ApplicationServices.GetRequiredService<IEventSubscribers>();
            applicationLifetime.ApplicationStarted.Register(eventSubscribers.Register);

            //END

            app.UseMvc();
        }
    }
}
