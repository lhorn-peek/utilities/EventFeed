﻿namespace Messaging.EventFeed
{
    public class EventHandlerSettings
    {
        public bool ProcessMissedEvents { get; set; }

        public bool TrackEvents { get; set; }
    }
}
