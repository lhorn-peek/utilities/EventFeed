﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PubNubMessaging.Core;

namespace Messaging.EventFeed.Common
{
    class PubnubJsonParser : IPubnubJsonParser
    {
        public Event ParseResult(string json, IJsonPluggableLibrary jsonLib)
        {
            if (!string.IsNullOrEmpty(json) && !string.IsNullOrEmpty(json.Trim()))
            {
                var deserializedMessage = jsonLib.DeserializeToListOfObject(json);
                if (deserializedMessage != null && deserializedMessage.Count > 0)
                {
                    var subscribedObject = deserializedMessage[0];
                    if (subscribedObject != null)
                    {
                        var resultActualMessage = jsonLib.SerializeToJsonString(subscribedObject);

                        return JsonConvert.DeserializeObject<Event>(resultActualMessage);
                    }
                }
            }

            return new Event();
        }
    }
}
