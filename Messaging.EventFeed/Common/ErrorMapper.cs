﻿using PubNubMessaging.Core;

namespace Messaging.EventFeed.Common
{
    internal static class ErrorMapper
    {
        public static PublishError ToPublishError(PubnubClientError pubnubError)
        {
            return MapTo<PublishError>(pubnubError);
        }

        public static SubscribeError ToSubscribeError(PubnubClientError pubnubError)
        {
            return MapTo<SubscribeError>(pubnubError);
        }


        private static TError MapTo<TError>(PubnubClientError pubNubError) where TError : ErrorBase, new()
        {
            if (pubNubError == null) return default(TError);

            return new TError
            {
                Message = pubNubError.Message,
                Description = pubNubError.Description,
                Exception = pubNubError.DetailedDotNetException
            };
        }
    }
}
