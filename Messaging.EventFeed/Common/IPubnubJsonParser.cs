﻿using PubNubMessaging.Core;

namespace Messaging.EventFeed.Common
{
    interface IPubnubJsonParser
    {
        Event ParseResult(string json, IJsonPluggableLibrary jsonLib);
    }
}