﻿using System;

namespace Messaging.EventFeed
{
    public struct Event
    {
        public Event(DateTimeOffset now, string eventName, int seq, object content, string publisherId)
        {
            OccuredAt = now;
            Name = eventName;
            SequenceNumber = seq;
            Content = content;
            PublisherId = publisherId;
        }

        public string PublisherId { get; set; }

        public DateTimeOffset OccuredAt { get; set; }

        public long SequenceNumber { get; set; }

        public string Name { get; set; }

        public object Content { get; set; }

       
    }
}
