﻿using System;
using System.Collections.Generic;
using Messaging.EventFeed.Common;
using Messaging.EventFeed.Interface;
using Newtonsoft.Json;
using PubNubMessaging.Core;

namespace Messaging.EventFeed
{
    public class EventHub : IEventHub
    {
        #region init

        private static Pubnub _pubnub;
        private IPubnubJsonParser _jsonParser;

        public EventHub(string pubKey, string subKey)
        {
            if (_pubnub == null)
            {
                _pubnub = new Pubnub(pubKey, subKey);
            }

            Configure();
        }

        private void Configure()
        {
            _jsonParser = new PubnubJsonParser();
        }

        #endregion

        public void Publish(string channel, string eventName, object content, Action<Event> successHandler, Action<PublishError> errorHandler)
        {
            var evt = new Event(DateTimeOffset.Now, eventName, 0, content, channel);

            _pubnub.Publish(channel, evt, successHandler, error =>
            {
                var pubError = ErrorMapper.ToPublishError(error);
                errorHandler(pubError);
            });
        }

        public void Listen(string channel, Action<Event> eventHandler, Action<SubscribeError> errorHandler)
        {
            Listen(channel, eventHandler, errorHandler, null);
        }

        public void Listen(string channel, Action<Event> eventHandler, Action<SubscribeError> errorHandler, EventHandlerSettings settings)
        {
            _pubnub.Subscribe<string>(channel, 
                successEvnt =>
                {
                    var eventsToHandle = new List<Event> {_jsonParser.ParseResult(successEvnt, _pubnub.JsonPluggableLibrary)};

                    if (settings != null)
                    {
                        HandleSettings(channel, ref eventsToHandle, settings);
                    }

                    eventsToHandle.ForEach(eventHandler);
                },
                connEvnt =>
                {
                    
                },
                errorEvnt =>
                {
                    var subError = ErrorMapper.ToSubscribeError(errorEvnt);
                    errorHandler(subError);
                });
        }


        #region private

        private void HandleSettings(string channel, ref List<Event> events, EventHandlerSettings settings)
        {
            //read history and track events(i.e save/update last processed event.OccuredOn )
        }

        #endregion
    }
}
