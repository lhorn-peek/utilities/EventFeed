﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messaging.EventFeed
{
    public abstract class ErrorBase
    {
        public string Message { get; set; }

        public string Description { get; set; }

        public Exception Exception { get; set; }
    }
}
