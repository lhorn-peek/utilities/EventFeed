﻿using System;

namespace Messaging.EventFeed.Interface
{
    public interface IEventHub
    {
        void Publish(string channel, string eventName, object content, Action<Event> successHandler, Action<PublishError> errorHandler);

        void Listen(string channel, Action<Event> eventHandler, Action<SubscribeError> errorHandler);

        void Listen(string channel, Action<Event> eventHandler, Action<SubscribeError> errorHandler, EventHandlerSettings settings);

    }

}
