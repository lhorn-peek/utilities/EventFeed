# EventFeed

Library for Publishing/Subscribing to an Event Feed.
Current implementation is a wrapper around PubNub realtime messaging network (https://www.pubnub.com/).
The example is in Asp.Net Core Web Application (.NET Framework). Look at the Startup.cs class and the Events folder for implementation details 

#ToDo
- Add event tracking to enable the replay of missed events
